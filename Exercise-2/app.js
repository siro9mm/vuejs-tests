new Vue({
	el: '#exercise',
  data: {
  	value: ""
  },
  methods: {
  	showAlert: function(event) {
    	alert("This is an alert!")
    },
    storeInput: function(event) {
    	this.value = event.target.value;
    }
  }
})