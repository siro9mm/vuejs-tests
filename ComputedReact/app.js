new Vue({
    el: '#app',
    data: {
      counter: 0,
      secondCounter: 0
    },
    computed: {
        output: function() {
          console.log('Computed');
          return this.counter > 5 ? 'Computed: Greater than 5' : 'Computed: Smaller than 5';
      }
    },
    watch: {
    		output: function(value) {
        		console.log('Watch');
        		var vm = this;
            setTimeout(function() {
            		vm.counter = 0;
            },2000);
        
        }
    },
    methods: {
        result: function() {
          console.log('Method');
          return this.counter > 5 ? 'Method: Greater than 5' : 'Method: Smaller than 5';
      }
    }
  })