new Vue({
	el: '#exercise',
    data: {
  	    name: 'Esprokotoy',
        age: 40,
        image: 'http://2.bp.blogspot.com/--mqQKXiTD0E/UIURXCYUxUI/AAAAAAAAHuQ/8jMmIdkdLX8/s1600/cats_desktop_1920x1200_hd-wallpaper-817100.jpeg',
    },
    methods:	{
  	    outputRandom: function() {
    	    return Math.random();
        },
        inputName: function(event) {
        	this.name =  event.target.value;
        },
        printName: function() {
        	return this.name;
        }
    }
})